package net.walischewski.bpmn.st;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Simple Task")
public class SimpleTaskApp extends ServletProcessApplication {
  // intentionally left empty
}
